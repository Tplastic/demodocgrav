---
title: 'WireShark Utilisation et installation'
media_order: 'imageTop.jpg,indextelecharge.jpg'
taxonomy:
    category:
        - docs
visible: true
---

![](imageTop.jpg)

Dans cette section, nous allons voir comment installer Wireshark et les bases de l'utilisation de cet outil.

### Installation !

En premier lieu, cliqué sur ce [lien](https://www.wireshark.org/#download) pour télécharger le logiciel.
Vous devrez téléchargé la version qui vous convient selon votre système d'exploitation.

![](indextelecharge.jpg)

Par la suite, vous devrez l'exécutable et cochez les options qui vous conviennent selon vos besoins.

Prenez quelques instant pour observer l'interface et vous familiariser aux différents options que vous offrent wireShark.

### Utilisation de WireShark

Rien de mieux qu'une vidéo explicative sur WireShark pour comprendre les bases d'un logiciel. Donc, pour bien apprendre les bases d'utilisation du logiciel, regarder ce tutoriel : 

Tutoriel Français ! :
[plugin:youtube](https://www.youtube.com/watch?v=LkTuYaWZsrs)

Tutoriel Anglais !: 
[plugin:youtube](https://www.youtube.com/watch?v=TkCSr30UojM)

Il s'agit d'une introduction à ce logiciel extraordinaire !
**Avec cette base et un peu de temps à travailler sur ce logiciel, vous deviendrai des pros du réseaux et des paquets !**

[Retour à la documentation de WireShark](/wireshark)

[ ^ Retour au haut de la page ^]()

_sources: https://www.youtube.com/watch?v=LkTuYaWZsrs_