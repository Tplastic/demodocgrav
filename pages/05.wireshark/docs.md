---
title: WireShark
media_order: 'wireshark_logo.png,index.jpg,index2.jpg,interface.jpg,img1Coder.jpg,th.jpg'
taxonomy:
    category:
        - docs
visible: true
---

## Objectif de documentation - WireShark


![](wireshark_logo.png)

WireShark est l'un des plus grand analysateur de paquets libre et gratuit. C'est pourquoi une documentation sur le sujet pourrait être importante pour de futurs utilisateurs ou simplement pour les curieux.
Pour mieux comprendre les notions qui suiveront, voici un shéma explicant le flux d'information qui circule sur internet. Vous comprenderez mieux les données reçu par l'analyse des paquets sur WireShark.

![](th.jpg)

<u>### Informations Générales<u>
Très accessible pour les utilisateurs grâce à l'interface utilisateur.
Ce logiciel est utilisé dans le dépannage et l'analyse de réseaux informatique. WireShark est aussi fréquemment utilisé à des fins éducatives
puisqu'il est gratuit. Il existe plusieurs versions et il est disponible sur de nombreux environnements pour permettre aux utilisateurs de différentes plateformes d'avoir accès aux fonctionnalités du logiciel.
Voici le lien pour le site officiel du logiciel : [](https://www.wireshark.org/)
#### Création de WireShark

Gerald Combs :
![](img1Coder.jpg)

La première de version de WireShark a été développée en 1999 aux Étas-Unis. **Gerald Combs**, un  diplômé de l'université du Missouri-Kansas City, travaillait dans une équipe d'un fournisseur d'accès à Internet.
Depuis ce temps, le logiciel offres plus de documentations, de stabilités et d'options pour les utilisateurs.

Le code source du logiciel a été codé dans les languages suivants :
* C
* C++
* Utilisation de -> Qt

#### Principales utilisations de Wireshark 

Wireshark permet d'avoir un interface de gestion pour un réseau, d'avoir des l'informations précises sur ce qui se passe dans votre réseau.

**Exemple d'interface sur WireShark :**

![](interface.jpg)

Le logiciel permet aussi de nombreuses autres utilitées : 

* Capturer des données de vos paquets dans le moment présent
* Importer vos propres paquets à partir de fichier texte
* Afficher les données de paquet et les informations de protocoles
* Sauvegarder les données de vos paquets capturés
* Afficher vos paquets en cours d'exécutions
* Filtrer vos différents paquets
* Recherche de paquet
* Coloriser vos paquets
* Générer des statistiques réseaux

Avec un peu d'expérience avec Wireshark, vous pourrez faire de grandre chose ! et régler vos problèmes de réseaux !



#### Suite installation et fonctionnement de WireShark

[**Installation et focntionnement de WireShark**](../wireshark-utilisation-et-installation)


[**Retourn au haut de la page :**](../wireshark)

_sources : _ https://www.wireshark.org/
_https://fr.wikipedia.org/wiki/Wireshark_